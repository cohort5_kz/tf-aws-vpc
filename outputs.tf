output "vpc_id_tf_aws_vpc" {
  description = "The VPC ID of mbpro-tf-dev27n"
  value = aws_vpc.main.id
}

output "rtb_id_tf_aws_vpc" {
  description = "The Route Table ID of mbpro-tf-dev27n"
  value = aws_vpc.main.default_route_table_id
}

output "nac_id_tf_aws_vpc" {
  description = "The ACL ID of mbpro-tf-dev27n"
  value = aws_vpc.main.default_network_acl_id
}

output "ipv4_addr_tf_aws_vpc" {
  description = "The IPv4 Address of mbpro-tf-dev27n"
  value = aws_vpc.main.cidr_block
}

output "public_subnet_cidrs_tf_aws_vpc" {
  description = "Public Subnets cirds of mbpro-tf-dev27n"
  value = aws_subnet.public_subnet_cidrs[*].cidr_block
}