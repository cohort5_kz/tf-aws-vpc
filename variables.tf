variable "vpc_cidr_block" {
  type = string
  description = "CIDR Block for tf-aws-vpc"
  default = "172.96.0.0/16"
}

variable "public_subnet_cidrs" {
 type        = list(string)
 description = "Public Subnet CIDR values"
 default     = ["172.96.1.0/24", "172.96.2.0/24", "172.96.3.0/24"]
}

variable "private_subnet_cidrs" {
 type        = list(string)
 description = "Private Subnet CIDR values"
 default     = ["172.96.4.0/24", "172.96.5.0/24", "172.96.6.0/24"]
}

variable "azs" {
  type = list(string)
  description = "Availability Zones"
  default = ["ap-southeast-1a","ap-southeast-1b","ap-southeast-1c"]
}