
provider "aws" {
  shared_config_files      = ["/home/vagrant/.aws/config"]
  shared_credentials_files = ["/home/vagrant/.aws/credentials"]
  profile                  = "terraform-user"
  region                   = "ap-southeast-1"
  alias                    = "aws-ecr-user"
}