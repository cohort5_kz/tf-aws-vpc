resource "aws_vpc" "main" {
  cidr_block       = var.vpc_cidr_block
  instance_tenancy = "default"

  tags = {
    Name = "tf-aws-vpc"
  }
}

#####Creating public and private subnets#####

resource "aws_subnet" "public_subnet_cidrs" {
  count = length(var.public_subnet_cidrs)
  vpc_id = aws_vpc.main.id
  cidr_block = element(var.public_subnet_cidrs, count.index)
  availability_zone = element(var.azs, count.index)
   
 tags = {
   Name = "Public Subnet ${count.index + 1}"
 }
}

resource "aws_subnet" "private_subnet_cidrs" {
  count = length(var.private_subnet_cidrs)
  vpc_id = aws_vpc.main.id
  cidr_block = element(var.private_subnet_cidrs, count.index)
  availability_zone = element(var.azs, count.index)
   
 tags = {
   Name = "Private Subnet ${count.index + 1}"
 }
}

#####Creating Internet Gateway#####

resource "aws_internet_gateway" "gw" {
 vpc_id = aws_vpc.main.id
 
 tags = {
   Name = "Project tf-aws-vpc IG"
 }
}

#####Creating 2nd Route Table#####

resource "aws_route_table" "second_rt" {
 vpc_id = aws_vpc.main.id
 
 route {
   cidr_block = "0.0.0.0/0"
   gateway_id = aws_internet_gateway.gw.id
 }
 
 tags = {
   Name = "2nd Route Table"
 }
}

#####Associating Public Subnets to the 2nd Route Table#####

resource "aws_route_table_association" "public_subnet_asso" {
 count = length(var.public_subnet_cidrs)
 subnet_id      = element(aws_subnet.public_subnet_cidrs[*].id, count.index)
 route_table_id = aws_route_table.second_rt.id
}

#####Creating Private Route Table#####

resource "aws_route_table" "private_rt" {
 vpc_id = aws_vpc.main.id
 
 tags = {
   Name = "Private Route Table"
 }
}

#####Associating Private Subnets to the Private Route Table#####

resource "aws_route_table_association" "private_subnet_asso" {
 count = length(var.private_subnet_cidrs)
 subnet_id      = element(aws_subnet.private_subnet_cidrs[*].id, count.index)
 route_table_id = aws_route_table.private_rt.id
}

#####Allow SSH Security Group#####
resource "aws_security_group" "tf-aws-sg-public" {
  name = "allow ssh"
  description = "Allow ssh inbound traffic and outbound traffic"
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "allow_ssh"
  }
}

resource "aws_vpc_security_group_ingress_rule" "tf-aws-sg-public-allow-ssh" {
  security_group_id = aws_security_group.tf-aws-sg-public.id
  cidr_ipv4 = "0.0.0.0/0"
  from_port = 22
  to_port = 22
  ip_protocol = "tcp"
}

# resource "aws_vpc_security_group_egress_rule" "tf-aws-sg-public-allow-all-traffic" {
#   security_group_id = aws_security_group.tf-aws-sg-public.id
#   cidr_ipv4 = "0.0.0.0/0"
#   ip_protocol = "-1"
# }
#####Allow SSH Security Group#####

#####CreateKeyPair#####
# resource "aws_key_pair" "tf-key" {
#   key_name   = "tf-aws-vpc-key"
#   public_key = file("/home/vagrant/tf-aws-vpc/tf-aws-vpc-key.pub") 
# }
#####End of CreateKeyPair#####

#####Creating EC2 Host#####
resource "aws_instance" "nas_1" {
  ami           = "ami-0b287aaaab87c114d"
  instance_type = "t2.micro"
  subnet_id     = "${aws_subnet.public_subnet_cidrs[0].id}"
  associate_public_ip_address = "true"
  vpc_security_group_ids = [ aws_security_group.tf-aws-sg-public.id ]
  key_name      = "my-key" #this key is already created at AWS
  # key_name      = aws_key_pair.tf-key.key_name
  tags = {
    Name = "PUBLIC 1"
  }
}

# resource "aws_instance" "nas_2" {
#   ami           = "ami-0b287aaaab87c114d"
#   instance_type = "t2.micro"
#   subnet_id     = "${aws_subnet.public_subnet_cidrs[1].id}"
#   # associate_public_ip_address = "true"
#   vpc_security_group_ids = [ aws_security_group.tf-aws-sg-public.id ]  
#   key_name      = "tf-aws-vpc" #this key is already created at AWS
#   tags = {
#     Name = "NAS 2"
#   }
# }
#####End of Public EC2 Instances#####

#####Start of Allow SSH Security Group for Private EC2#####
resource "aws_security_group" "tf-aws-sg-private" {
  name = "allow ssh for private subnet"
  description = "Allow ssh inbound traffic and outbound traffic"
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "allow_ssh_for_private_subnet"
  }
}

resource "aws_vpc_security_group_ingress_rule" "tf-aws-sg-private-allow-ssh" {
  security_group_id = aws_security_group.tf-aws-sg-private.id
  cidr_ipv4 = "172.96.1.0/24" #Only allow from that subnet
  from_port = 22
  to_port = 22
  ip_protocol = "tcp"
}

# resource "aws_vpc_security_group_egress_rule" "tf-aws-sg-private-allow-all-traffic" {
#   security_group_id = aws_security_group.tf-aws-sg-private.id
#   cidr_ipv4 = "0.0.0.0/0"
#   ip_protocol = "-1"
# }
#####End of Allow SSH Security Group for Private EC2#####

#####Create Private EC2 Instances#####
resource "aws_instance" "private_nas_1" {
  ami           = "ami-003c463c8207b4dfa" #Ubuntu Linux AMI
  instance_type = "t2.micro"
  subnet_id     = "${aws_subnet.private_subnet_cidrs[0].id}"
  associate_public_ip_address = "true"
  vpc_security_group_ids = [ aws_security_group.tf-aws-sg-private.id ]
  key_name      = "my-key" #this key is already created at AWS
  # key_name = aws_key_pair.tf-key.key_name
  tags = {
    Name = "PRIVATE 4"
  }
}

resource "aws_instance" "private_nas_2" {
  ami           = "ami-003c463c8207b4dfa" #Ubuntu Linux AMI
  instance_type = "t2.micro"
  subnet_id     = "${aws_subnet.private_subnet_cidrs[1].id}"
  associate_public_ip_address = "true"
  vpc_security_group_ids = [ aws_security_group.tf-aws-sg-private.id ]  
  key_name      = "my-key" #this key is already created at AWS
  # key_name = aws_key_pair.tf-key.key_name
  tags = {
    Name = "PRIVATE 5"
  }
}
#####End of Create Private EC2 Instances#####

resource "tls_private_key" "this" {
  algorithm     = "RSA"
  rsa_bits      = 4096
}

resource "aws_key_pair" "this" {
  key_name      = "my-key"
  public_key    = tls_private_key.this.public_key_openssh

  provisioner "local-exec" {
    command = <<-EOT
      echo "${tls_private_key.this.private_key_pem}" > my-key.pem
    EOT
  }
}

####allocate EIP and NATGW#####
resource "aws_eip" "nat_gw" {
  vpc = true

}